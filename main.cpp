#include <stdio.h>
#include <string.h>
#include <iostream>
#include "heap_allocation.h"
using std::cout;
using std::endl;
int main()
{
    Heap heap(20); // in bytes;
    char* str = (char*)heap.malloc(sizeof(char)*5);
    for (int i = 0; i < 5; i++){
        str[i] = 97+i;
    }
    cout << "string: " << str << endl;
    heap.memmove(str+3, str+1, 1);
    cout << "memmove function for string: " << str << endl;
    heap.free(str);
    return 0;
}
