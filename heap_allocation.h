#ifndef HEAP_ALLOCATION_H
#define HEAP_ALLOCATION_H
#include <map>

using ulong = unsigned long;
class Heap{
private:
    enum {DEF_SIZE = 1};
    ulong heapSize;
    ulong reserved;

    char * heapBlock;
    std::map<void *, ulong> mem;

    void showAddresses(ulong size);
    ulong getBytes(void * address);
    void* search(ulong insertion);
    bool consecutive(void* address, ulong insertion, ulong previous);
    void changeBytes(void* address, ulong changed);
    void memerr();
public:
    Heap(ulong size = DEF_SIZE);
    ~Heap();
    void* malloc(ulong insertion = 1);
    void free(void * type);
    void* realloc(void *pointer, ulong insertion);
    void showMap();
    void* memmove(void* dest, void* src, ulong num);
    void* memcpy(void* dest, const void* src, ulong num);
    void* memset(void* ptr, int val, ulong num);
    void* memchr(const void *ptr, int val, ulong num);
};
#endif // HEAP_ALLOCATION_H
