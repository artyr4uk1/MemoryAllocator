#include "heap_allocation.h"
#include <new>
#include <iostream>
#include <cstdlib>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

void Heap::showAddresses(ulong size)
{
    cout << "\t\tAddresses --------->";
    for (ulong i = 0; i < size; i++){
        if (i%6 == 0) cout << "\n";
        cout << (void*)&heapBlock[i] << "   ";
    }
    cout << "\n\n";
}

ulong Heap::getBytes(void *address)
{
    auto iter = mem.find(address);
    if (iter != mem.end()){
        return iter->second;
    }
    return 0;
}


bool Heap::consecutive(void * address, ulong insertion, ulong previous)
{
    if (((char*)address + insertion) >= &heapBlock[heapSize]) {
        return false;
    }

    char* start = ((char*)address + previous);
    ulong temp = 0;
    ulong countBytes = 0;

    do {
        temp = getBytes(start);
        if (temp != 0) return false;
        countBytes++;
        start++;
    } while (countBytes < insertion);
    if (countBytes == insertion){
        return true;
    }
    return false;
}

void *Heap::search(ulong insertion)
{
    char * returningAddress = nullptr; // address that will be returned
    char * point = &heapBlock[0]; // an arrow address
    ulong countBytes = 1; // counting a free passed bytes
    bool check = true;
    bool noIfStatement = true;

    while (point < &heapBlock[heapSize]){
        if (!mem.empty()){
            while(check){
                for (auto i = mem.begin(); i != mem.end(); i++){
                    if (point == i->first){
                        point = point + i->second;
                        returningAddress = point;
                        noIfStatement = false;
                        countBytes = 1; continue;
                    } else {
                        if (countBytes == insertion){
                            if (noIfStatement == true) returningAddress = &heapBlock[0];
                            check = false; break;
                        }
                        point++; countBytes++;
                    }
                }
            }
            break;
        } else {
            return point;
        }
    }
    if (returningAddress == nullptr){
        memerr();
    }
    return returningAddress;
}

void Heap::changeBytes(void *address, ulong changed)
{
    auto iter = mem.find(address);
    if (iter != mem.end()){
        iter->second = changed;
    } else {
        cerr << "Error: Not found map key " << address << "!" << endl;
        exit(EXIT_FAILURE);
    }
}

void Heap::memerr()
{
    cerr << "Error: The required space is too big "
                 "or not enough memory space!\n" << endl;
    exit(EXIT_FAILURE);
}

Heap::Heap(ulong size)
{
    heapSize = size;
    reserved = 0;
    heapBlock = (char*)::malloc(heapSize * sizeof(char));
    if (heapBlock == NULL){
        cout << "Error: Memory for heap not allocated!" << endl;
        exit(EXIT_FAILURE);
    }
    showAddresses(heapSize);
}

Heap::~Heap()
{
    ::free(heapBlock);
}

void *Heap::malloc(ulong insertion)
{
    if (insertion == 0){
        return NULL;
    } else if(insertion > (heapSize - reserved)){
       memerr();
    }
    void * address = search(insertion);
    mem.insert(std::pair<void *, ulong>(address, insertion));
    reserved += insertion;
    return address;
}

void Heap::free(void *type)
{
    auto iter = mem.find(type);
    if (iter != mem.end()){
        reserved -= iter->second;
        mem.erase(iter);
        cout << "free(" << type << ")" << endl;
    } else {
        cerr << "The memory haven't been allocated by this address!" << endl;
        return;
    }
}

void *Heap::realloc(void *pointer, ulong insertion) // waiting for check
{
    if (insertion == 0){
        free(pointer);
        return NULL;
    } else if(insertion > (heapSize)){
       memerr();
    }

    ulong previous = getBytes(pointer);

    if (previous == 0){ // created a new pointer
        pointer = malloc(insertion);
        return pointer;
    } else { // 3 cases
        if (insertion == previous){ // equal
            return pointer;
        } else if (insertion > previous){ // increase array
            ulong additionalSize = insertion - previous;
            if (reserved + additionalSize > heapSize){
                memerr();
            } else if(consecutive(pointer, insertion, previous)){ // check for consecutive addresses
                changeBytes(pointer, insertion);
                reserved += additionalSize;
                return pointer;
            } else {
                std::map<void *, ulong> temp;
                temp.insert(std::pair<void *, ulong>(pointer, previous));
                free(pointer);
                char * newPointer = (char*)malloc(insertion); // if there will be error exit() called
                // copy information from previousious address to new address by byte
                memmove(newPointer, pointer, previous);
                return newPointer;
            }
        } else { // decrease array
            changeBytes(pointer, insertion);
            reserved -= previous - insertion; // substract a difference
        }
        return pointer;
    }
}

void Heap::showMap()
{
    cout << "\n";
    cout << "\tReserved memory:\n" <<
            "\taddress\t\tbytes" << endl;
    for (auto i = mem.begin(); i != mem.end(); i++){
        cout << "\t" << i->first << "\t" << i->second << endl;
    }
    cout << "\nSize of heap: " << heapSize << " bytes" <<
            "\nReserved size of heap: " <<
            reserved << " bytes" << endl;
    cout << "\n";
}

void * Heap::memmove(void *dest, void *src, ulong num)
{
    if ((dest == NULL) || (src == NULL)){
        cerr << "Argument is NULL pointer!" << endl;
        return NULL;
    }

    char* ldest = (char*)dest;
    char* lsrc = (char*)src;

    char * tmp = (char*)malloc(sizeof(char)*num);
    if (tmp == NULL){
        cerr << "Error: Memory not allocated for tmp in Heap::memmove!" << endl;
        exit(EXIT_FAILURE);
    } else {
        for(ulong i = 0; i < num; i++){
            *(tmp + i) = *(lsrc + i);
        }
        for(ulong i = 0; i < num; i++){
            *(ldest + i) = *(tmp + i);
        }
        free(tmp);
    }
    return dest;
}

void* Heap::memcpy(void *dest, const void *src, ulong num)
{
    if ((dest == NULL) || (src == NULL)){
        cerr << "Argument is NULL pointer!" << endl;
        return NULL;
    }

    char* ldest = (char*)dest;
    const char* lsrc = (const char*)src;

    while(num){
        *(ldest++) = *(lsrc++);
        num--;
    }
    return dest;
}

void *Heap::memset(void *ptr, int val, ulong num)
{
    if (ptr == NULL){
        cerr << "Argument is NULL pointer!" << endl;
        return NULL;
    }
    unsigned char* lptr = (unsigned char*)ptr; // unsigned
    while (num) {
        *(lptr++) = val;
        num--;
    }
    return ptr;
}

void *Heap::memchr(const void *ptr, int val, ulong num)
{
    if (ptr == NULL){
        cerr << "Argument pointer in NULL!\n" << endl;
        return NULL;
    }
    unsigned char* lptr = (unsigned char*)ptr;
    while (num--)
        if (*lptr != (unsigned char)val)
            lptr++;
        else
            return lptr;

    return 0;
}
